<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Testing\Fluent\Concerns\Has;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $drugs = ["stramon","fanigan","pensilin"];
        $count = [230,85,60];
        for ($i=0; $i<3; $i++)
        {
            DB::table('drugs')->insert([
                'drug_title'=>$drugs[$i],
                'drug_all'=>$count[$i]
            ]);
        }

        DB::table('users')->insert([
            'name'=>'Axmedov Aziz',
            'alogin'=>'akhmedov',
            'password'=>Hash::make('12341234'),
            'role'=>1
        ]);
    }
}
