<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Doctor dashboard</title>
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('auth/app-assets/images/ico/apple-icon-60.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('auth/app-assets/images/ico/apple-icon-76.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('auth/app-assets/images/ico/apple-icon-120.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('auth/app-assets/images/ico/apple-icon-152.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('auth/app-assets/images/ico/favicon.ico')}}">
    <link rel="shortcut icon" type="image/png" href="{{asset('auth/app-assets/images/ico/favicon-32.png')}}">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/fonts/icomoon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/fonts/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/vendors/css/extensions/pace.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/css/bootstrap-extended.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/css/app.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/css/colors.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/css/core/menu/menu-types/vertical-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/css/core/menu/menu-types/vertical-overlay-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/app-assets/css/core/colors/palette-gradient.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('auth/assets/css/style.css')}}">
</head>
<body>

<br>
@if($retseptlar[0]->qrcode_scan == 0)
<div style="width: 100%; display: flex; justify-content: center">
<div class="col-xl-4 col-md-6 col-sm-12">
    <div class="card" style="height: auto;">
        <div class="card-body">
            <div class="card-block">
                <h4 class="card-title">{{$retseptlar[0]->patient}}</h4>
                <p class="card-text">Ushbu bemorga berilgan dorilar ro'yxati</p>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    Dori nomi : &nbsp;
                    <span class="tag tag-default tag-pill bg-primary">
{{--                        @if($retseptlar[0]->drug_name == 1)--}}
{{--                            stramon--}}
{{--                        @elseif($retseptlar[0]->drug_name == 2)--}}
{{--                            fanigan--}}
{{--                        @elseif($retseptlar[0]->drug_name == 3)--}}
{{--                            pensilin--}}
{{--                        @endif--}}
                        {{$retseptlar[0]->drug_title}}
                    </span>

                </li>
                <li class="list-group-item">
                    Dori dozasi :
                    <span class="tag tag-default tag-pill bg-info">{{$retseptlar[0]->drug_doze}}</span>

                </li>
                <li class="list-group-item">
                    Dori soni : &nbsp;&nbsp;
                    <span class="tag tag-default tag-pill bg-warning">{{$retseptlar[0]->drug_count}} dona</span>

                </li>

            </ul>
            <div class="card-block">
                <a href="{{route('sell.drug', $retseptlar[0]->asosiy)}}" class="btn btn-success">Dorini sotish</a>
            </div>
        </div>
    </div>
</div>
</div>
@elseif($retseptlar[0]->qrcode_scan == 1)
    <div style="width: 100%; display: flex; justify-content: center">
        <div class="col-xl-4 col-md-6 col-sm-12">
            <div class="card" style="height: auto;">
                <div class="card-body">
                    <div class="card-block">
                        <h4 class="card-title">{{$retseptlar[0]->patient}}</h4>
                        <span class="tag tag-default tag-pill bg-primary">Ushbu bemorga berilgan dorilar barchasi sotilgan</span>
                    </div>
                    <ul class="list-group">


                    </ul>
                    <div class="card-block">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<script src="{{asset('auth/app-assets/js/core/libraries/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/vendors/js/ui/tether.min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/js/core/libraries/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/vendors/js/ui/unison.min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/vendors/js/ui/blockUI.min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/vendors/js/ui/jquery.matchHeight-min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/vendors/js/ui/screenfull.min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/vendors/js/extensions/pace.min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/vendors/js/charts/chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/js/core/app-menu.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/js/core/app.js')}}" type="text/javascript"></script>
<script src="{{asset('auth/app-assets/js/scripts/pages/dashboard-lite.js')}}" type="text/javascript"></script>
</body>
</html>
