<div class="main-menu-content">
    <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">

        <li class=" nav-item">
            <a href="#">
                <i class="icon-file-add"></i>
                <span data-i18n="nav.page_layouts.main" class="menu-title">Retsept</span>
            </a>
            <ul class="menu-content {{(request()->routeIs('recipe.create') || request()->routeIs('recipe.index')) ? 'd-block' : ''}}">
                <li><a style="color: {{request()->routeIs('recipe.create') ? "#7366ff" : ""}}" href="{{route('recipe.create')}}" data-i18n="nav.page_layouts.1_column" class="menu-item">retsept yaratish</a>
                </li>
                <li><a style="color: {{request()->routeIs('recipe.index') ? "#7366ff" : ""}}" href="{{route('recipe.index')}}" data-i18n="nav.page_layouts.2_columns" class="menu-item">retseptlar ro'yxati</a>
                </li>
            </ul>
        </li>

        <li class=" nav-item">
            <a href="#">
                <i class="icon-basecamp"></i>
                <span data-i18n="nav.page_layouts.main" class="menu-title">Bazdagi dorilar</span>
            </a>
            <ul class="menu-content {{(request()->routeIs('drug.view')) ? 'd-block' : ''}}">
                <li><a style="color: {{request()->routeIs('drug.view') ? "#7366ff" : ""}}" href="{{route('drug.view')}}" data-i18n="nav.page_layouts.1_column" class="menu-item">dorilar ro'yxati</a>
                </li>
            </ul>
        </li>

    </ul>
</div>
