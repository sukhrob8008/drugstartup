@extends('doctors.layouts.master')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Bemorlarga siz tomoningizdan yozilgan retseptlar</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">

                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block card-dashboard">
                        <p>Add <code class="highlighter-rouge">.table-hover</code> to enable a hover state on table rows within a <code class="highlighter-rouge">&lt;tbody&gt;</code>.</p>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover mb-0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Dori nomi</th>
                                <th>Dozasi</th>
                                <th>Soni</th>
                                <th>Bemor</th>
                                <th>Qr code</th>
                                <th>Scan</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recipes as $key => $recipe)
                            <tr>
                                <th scope="row">{{$key}}</th>
                                <td>{{$recipe->drug_title}}</td>
                                <td>{{$recipe->drug_doze}}</td>
                                <td>{{$recipe->drug_count}}</td>
                                <td>
                                    <a href="{{route('show.retsept', $recipe->asosiy)}}">{{$recipe->patient}}</a>
                                </td>
                                <td>
                                    {{QrCode::size(300)->format('svg')->generate(route('show.retsept',$recipe->asosiy), public_path('doctors/qrcodes/'.$recipe->recipe_qrcode))}}
                                    <img width="70" height="70" src="{{asset('doctors/qrcodes/'.$recipe->recipe_qrcode)}}" alt="">
                                </td>
                                <td>{{$recipe->qrcode_scan}}</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
