@extends('doctors.layouts.master')
@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title" id="basic-layout-card-center">Bemorga yoziladigan dorilar</h4>
                    <a class="heading-elements-toggle"><i class="icon-ellipsis font-medium-3"></i></a>
                    <div class="heading-elements">
                        <ul class="list-inline mb-0">

                        </ul>
                    </div>
                </div>
                <div class="card-body collapse in">
                    <div class="card-block">
                        <div class="card-text">
                            <p>Ushbu yozilgan dorilar ro'yxati doimo nazoratda turadi.Ushbu yozilgan dorilar ro'yxati doimo nazoratda turadi.Ushbu yozilgan dorilar ro'yxati doimo nazoratda turadi.Ushbu yozilgan dorilar ro'yxati doimo nazoratda turadi.</p>
                        </div>
                        <form class="form" action="{{route('recipe.store')}}" method="post">
                            @csrf
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="eventRegInput1">Dori nomi</label>
                                    <select name="drug_name" id="projectinput6" name="budget" class="form-control">
                                        @foreach($drugs as $drug)
                                            <option value="{{$drug->id}}">{{$drug->drug_title}}</option>
                                        @endforeach
                                    </select>
{{--                                    <select class="form-select" name="drug_name" id="">--}}

{{--                                    </select>--}}
{{--                                    <input type="text" id="eventRegInput1" class="form-control" placeholder="nomlanishi" name="drug_name">--}}
                                </div>

                                <div class="form-group">
                                    <label for="eventRegInput2">Dozasi</label>
                                    <input type="text" id="eventRegInput2" class="form-control" placeholder="dozasi" name="drug_doze">
                                </div>

                                <div class="form-group">
                                    <label for="eventRegInput3">Soni</label>
                                    <input type="text" id="eventRegInput3" class="form-control" placeholder="nechta kerak" name="drug_count">
                                </div>

                                <div class="form-group">
                                    <label for="eventRegInput3">Bemor FIO</label>
                                    <input type="text" id="eventRegInput3" class="form-control" placeholder="bemor ismi familiyasi" name="patient">
                                </div>

                            </div>

                            <div class="form-actions center">
                                <button type="submit" class="btn btn-primary">
                                    <i class="icon-check2"></i> Retseptni yaratish
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
