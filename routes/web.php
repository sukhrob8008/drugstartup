<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::middleware(['middleware'=>'back'])->group(function (){
    Auth::routes([
        'register'=>false
    ]);
});

//Route::group(['prefix'=>'doctor','middleware'=>['auth','doctor']], function (){
    Route::get('/doctor',[\App\Http\Controllers\HomeController::class, 'doctorpanel'])->name('doctorpanel');
    Route::resource('/doctor/recipe', \App\Http\Controllers\Doctor\RecipeController::class);
    Route::get('/doctor/base', [\App\Http\Controllers\Doctor\RecipeController::class, 'drug_views'])->name('drug.view');
//});
Route::get('/drug/from/{recipe}',[\App\Http\Controllers\Doctor\RecipeController::class, 'show_drug'])->name('show.retsept');
Route::get('/drug/sell/{recipe}', [\App\Http\Controllers\Doctor\RecipeController::class, 'drug_update'])->name('sell.drug');



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
