<?php

namespace App\Http\Controllers\Doctor;

use App\Http\Controllers\Controller;
use App\Models\Recipe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $recipes = DB::select("select recipes.id as asosiy,recipes.drug_name,recipes.drug_doze,recipes.drug_count,recipes.patient,recipes.recipe_qrcode,recipes.qrcode_scan,drugs.id as ikki, drugs.id, drugs.drug_title, drugs.drug_all from recipes inner join drugs on recipes.drug_name=drugs.id");
//        dd($recipes);
        return view('doctors.recipe.index', compact('recipes'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $drugs = DB::select("select * from drugs");
        return view('doctors.recipe.create', compact('drugs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {


        $recipes = new Recipe();
        $recipes->drug_name = $request->drug_name;
        $recipes->drug_doze = $request->drug_doze;
        $recipes->drug_count = $request->drug_count;
        $recipes->patient = $request->patient;
        $qr_name = uniqid().".svg";
        $recipes->recipe_qrcode	= $qr_name;
        $recipes->qrcode_scan = 0;
        $recipes->save();
//        QrCode::size(300)->format('png')->generate(route('show.retsept',$recipes->id), public_path('doctors/qrcodes/'.$qr_name));
        return redirect()->route('recipe.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function show_drug(Recipe $recipe)
    {
//        dd($recipe);
        $retseptlar = DB::select("select recipes.id as asosiy,recipes.drug_name,recipes.drug_doze,recipes.drug_count,recipes.patient,recipes.recipe_qrcode,recipes.qrcode_scan,drugs.id as ikki, drugs.id, drugs.drug_title, drugs.drug_all from recipes inner join drugs on recipes.drug_name=drugs.id where recipes.id=?",[$recipe->id]);
//        dd($retseptlar[0]);
        return view('main.view', compact('retseptlar'));
    }

    public function drug_views()
    {
        $drugs = DB::select("select * from drugs");
        return view('doctors.drug.index', compact('drugs'));
    }

    public function drug_update(Recipe $recipe)
    {
//        dd($recipe);
        $sell_drug = $recipe->drug_count;
        $base_count = DB::select("select drug_all from drugs where id=?",[$recipe->drug_name]);
        $result = $base_count[0]->drug_all-$sell_drug;
//        dd($result);
        DB::select("update drugs set drug_all=? where id=?",[$result,$recipe->drug_name]);
        DB::select("update recipes set qrcode_scan=1 where id=?",[$recipe->id]);
        return redirect()->route('recipe.index');
    }
}
